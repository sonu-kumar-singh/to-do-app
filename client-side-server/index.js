
let btn = document.getElementsByClassName("addBtn")
btn[0].addEventListener("click", myFun)
function myFun() {
    const li = document.createElement("li");
    const button = document.createElement("button");
    button.classList.add("removeButton");
    button.innerHTML = "Delete";
   const inputValue = document.getElementById("myInput").value;
    // Adding 'listed' class to li element
   li.classList.add("listed"); 

    // creating checkbox element
    const checkbox = document.createElement('input');
             
    // Assigning the attributes
    // to created checkbox
    checkbox.type = "checkbox";
    checkbox.classList.add("checkBox")

    li.appendChild(checkbox)
   // Adding input value to li
   li.append(inputValue);
   // Adding button to li
   li.appendChild(button);
  if (inputValue === '') {
    alert("You must write something!");
  } else {
    document.getElementById("myUl").appendChild(li);
  }
//   to give input field placeholder after adding the to do
   document.getElementById("myInput").value = '';
}



// let ul = document.getElementById('myUl');
// console.log(ul);
// let removeLi = document.getElementsByClassName('removeButton');
// console.log(removeLi)
// removeLi[0].addEventListener('click', console.log('button clicked'));


